﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallSignal : MonoBehaviour
{
    public int collisionCount = 0;
   
   
   //when this object collides with another
   void OnCollisionEnter(Collision collision){
       
       //check if its something we want to count as a collision
       if (collision.gameObject.tag == "okToHit"){
            collisionCount++;
        }

        //check if its something we don't want to count as a collision
        if (collision.gameObject.tag != "okToHit" && collision.gameObject.name != "West Wall" && collision.gameObject.tag != "Player"){
            collisionCount--;
        }
         
   }

    
    //gives a count of the collisions
    public int GetCount(){
        return collisionCount;
    }
    
}

