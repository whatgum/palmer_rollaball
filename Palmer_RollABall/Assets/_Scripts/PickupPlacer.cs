﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupPlacer : MonoBehaviour
{
    private int numOfPickups = 13;
    private float baseYPosition = .4246265f;
    public GameObject pickup;
    private float baseXPosition = 9.45f;
    private float baseZPosition = 4.75f;
    private float baseZPositionBottom = 9.45f;

    //creates new pickups for each new level
    public void PlaceNewPickups(){

        //increases amount of pickups by two and the ranges they can be placed
        numOfPickups = numOfPickups * 2;
        baseXPosition = baseXPosition + 9.45f;
        baseZPosition = baseZPosition + 8.75f;
        baseZPositionBottom = baseZPositionBottom + 9.45f;

        //place every pickup
        for (int i = 0; i < numOfPickups; i++){
            GameObject pick = Instantiate(pickup, new Vector3(Random.Range(-baseXPosition,baseXPosition),baseYPosition,Random.Range(-baseZPositionBottom, baseZPosition)), Quaternion.identity);
            pick.transform.parent = gameObject.transform;
        }
    }
}
