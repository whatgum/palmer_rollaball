﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelProceduration : MonoBehaviour
{   
    public PickupPlacer picker;
    public OrangeWallCreator orangeWalls;
    public new CameraController camera;
    public GameObject walls;
    public PlayerController player;
    float XScale = 2;
    float ZScale = 2;
    private readonly int rateOfExpansion = 1;
    

    // Update is called once per frame
    void Update()
    {
        //if everything but the ground is gone
        if (transform.childCount == 1)
        {
            //create a new scale
            Vector3 scale = new Vector3(XScale, 1f, ZScale);
            NewGreenWalls(scale);
            scale = new Vector3(XScale * 2, 2f, ZScale * 2);
            ScalePlane(scale);

            XScale = XScale + rateOfExpansion;
            ZScale = ZScale + rateOfExpansion;

            //remake everything to the new scale
            NewOrangeWalls();
            camera.UpdateCameraOffset();
            picker.PlaceNewPickups();
            player.SetNewLevel(true);
            player.ResetPosition();
        }
    }
    //scales the plane
    private void ScalePlane(Vector3 scale)
    {
        
        transform.GetChild(0).transform.localScale = scale;
        
    }

    //creates new green walls
    private void NewGreenWalls(Vector3 scale)
    {
        scale = new Vector3(scale.x, .9f, scale.z);
        GameObject greenWalls  = Instantiate(walls, new Vector3(0, 0, 0), Quaternion.identity);
        greenWalls.transform.parent = gameObject.transform;
        gameObject.transform.GetChild(1).transform.localScale = scale;
        gameObject.transform.GetChild(1).name = "GreenWalls";
        
    }

    //creates new orange walls
    private void NewOrangeWalls()
    {
        orangeWalls.CreateFirstWall(ZScale);
    }

    //destroys the green walls
    public void DestroyGreenWalls()
    {
        Destroy(gameObject.transform.GetChild(1).gameObject);
    }
}
