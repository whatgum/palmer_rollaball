﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrangeWallCreator : MonoBehaviour
{
    private float stretch = 2f;
    public bool finished = false;
    public GameObject OrangeWall;
    private readonly float baseXPosition = -3.551f;
    private readonly float baseYPosition = .5f;
    private readonly float baseZPosition = 7.75f;
    private readonly float baseDistanceForNewOrangeWall = 9.75f;
    private readonly int levelOut = 2;
    private readonly float xDistanceFromOrangeWallIfRotated = 1.75f;
    private readonly float zDistanceFromOrangeWallIfRoated = 2.25f;
    private int time = 0;

    //provides a lateupdates
    private void LateUpdate(){

        //this entire thing exists because the walls need to collide first
        //and if you just put cleanup without a buffer of time
        //they all get destroyed as they haved collided yet
        time++;
        
        if (time == 10 && finished == false){
            cleanup();
            time = 0;
        }

        if (time > 40 && finished == true){
            finished = false;
            time = 0;
        }
    }

    //destroys excess walls that have not collided
    private void cleanup(){

        foreach (Transform child in transform )
        {
            
            wallSignal wall = child.gameObject.GetComponent(typeof(wallSignal)) as wallSignal;
            
            if (wall.GetCount() == 0){
                
                Destroy(child.gameObject);
            
            }
            
        }
    }

    //creates the first wall all other walls are based off
    public void CreateFirstWall(float Scale)
    {
        //the exact amount the wall is away from the north green wall
        float trueZPosition = baseZPosition + (baseDistanceForNewOrangeWall* (Scale- levelOut));
        CreateAWall(trueZPosition);

        gameObject.name = "OrangeWalls"; 
        CreateRemainingWalls(trueZPosition);
    }

    //create the walls based off the first wall
    private void CreateRemainingWalls(float zPosition){
       
        CreateGreenSwitchBarrier(zPosition);
        CreateOrangeSwitchBarrier(zPosition);

        //this is so cleanup doesn't destroy walls before they collide
        finished = true;
    }

    //creates a barrier to the green switch
    private void CreateGreenSwitchBarrier(float zPosition){
        
        GameObject orangeWall = CreateAWall(zPosition);
        RotateRight(orangeWall);
        
        //because I do not know the exact amount of walls nessecary, do it a set amount of arbitrary times
        float target = zPosition * 2f;
        for (int i = 0; i < target; i++)
        {

            Vector3 position = orangeWall.transform.position;
            orangeWall = CreateAWall(position);
            ContinueRotatedRight(orangeWall);
            

        }
     

    }

    //creates a barrier to the orange switch
    private void CreateOrangeSwitchBarrier(float zPosition){
        
        GameObject orangeWall = CreateAWall(zPosition);
        RotateLeft(orangeWall);

        //tag these walls gates for when they get destroyed seperately
        orangeWall.gameObject.tag = "gate";

        float target = zPosition * 2f;

        //because I do not know the exact amount of walls nessecary, do it a set amount of arbitrary times
        for (int i = 0; i < target; i++){
            
            Vector3 position = orangeWall.transform.position;
            orangeWall = CreateAWall(position);
            ContinueRotatedLeft(orangeWall);
            orangeWall.gameObject.tag = "gate";
        }

    }

    //creates a new wall with a z position
    private GameObject CreateAWall(float zPosition){
        
        GameObject orangeWall = Instantiate(OrangeWall, new Vector3(baseXPosition - stretch, baseYPosition, zPosition),Quaternion.identity );
        
        orangeWall.transform.parent = gameObject.transform;
        orangeWall.transform.name = "Orange Wall";
       
        return orangeWall;
    }

    //creates a new wall with a position vector
    private GameObject CreateAWall(Vector3 position){

        GameObject orangeWall = Instantiate(OrangeWall, position, Quaternion.identity);
       
        orangeWall.transform.parent = gameObject.transform;
        orangeWall.transform.name = "Orange Wall";
        
        return orangeWall;
    }

    //destroys the orange walls
    public void DestroyOrangeWalls()
    {
        //stretches the walls every time they are destoryed
        stretch = stretch + 5f;

        //destroy all children
        foreach (Transform child in transform )
        {
            
            Destroy(child.gameObject);

            
        }
    }

    //rotates wall 90 degrees
    private void Rotate(GameObject wall){

        wall.transform.Rotate(0f, 90f, 0f);

    }

    //rotates wall left
    private void RotateLeft(GameObject wall){
        
        wall.transform.position = wall.transform.position + new Vector3(-xDistanceFromOrangeWallIfRotated,0f,-zDistanceFromOrangeWallIfRoated);
       
        Rotate(wall);
    }

    //rotates wall right
    private void RotateRight(GameObject wall){
        
        wall.transform.position = wall.transform.position + new Vector3(+xDistanceFromOrangeWallIfRotated,0f,-zDistanceFromOrangeWallIfRoated);
    
        Rotate(wall);
    }

    //creates a new wall piece for the wall that is being continued while rotated right
    private void ContinueRotatedRight(GameObject wall){
        wall.transform.position = wall.transform.position + new Vector3(+xDistanceFromOrangeWallIfRotated,0f,0f);
        Rotate(wall);

    }

    //creates a new wall piece for the wall that is being continued while rotated left
    private void ContinueRotatedLeft(GameObject wall){
        wall.transform.position = wall.transform.position + new Vector3(0f, 0f, +zDistanceFromOrangeWallIfRoated);
        RotateLeft(wall);
    }
}
