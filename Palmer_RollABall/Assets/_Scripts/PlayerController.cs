﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    bool nextLevel = false;
    private int amount = 13;
    public float speed = 50;
    private new Rigidbody rigidbody;
    private int count;
    public Text countText;
    public Text winText;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (nextLevel == true){
            winText.text = "";
            nextLevel = false;
        }
    }

    private void FixedUpdate()
    {
        //wow I am coding again, what time to be alive
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rigidbody.AddForce(movement * speed);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick up"))
        {
            Destroy(other.gameObject);
            count++;
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= amount)
        {
            winText.text = "Go To next Level";

            //find all the objects tagged as gate and destroy them
            GameObject[] gates = GameObject.FindGameObjectsWithTag("gate");
            foreach (GameObject gate in gates)
            {
                Destroy(gate);
            }

            //reset count and increase amount
            count = 0;
            amount = amount * 2;
        }
    }

    //when a new level is made resests win text
    public void SetNewLevel(bool truth){
        nextLevel = truth;
    }

    //resets the position of the player for the new level
    public void ResetPosition()
    {
        gameObject.transform.position = new Vector3(0f, 0f, 0f);
        SpeedUp();
    }

    private void SpeedUp()
    {
        speed = speed * 1.5f;

    }
}
