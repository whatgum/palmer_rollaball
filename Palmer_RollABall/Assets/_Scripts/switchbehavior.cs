﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchbehavior : MonoBehaviour
{
    public GameObject switcher;
    public levelProceduration level;
    public OrangeWallCreator orangeWallContainer;
    bool once = true;
   
    //when this switch is sat on
    private void OnTriggerEnter(Collider other)
    {

        //makes sure this only happens once
        if (once == true)

        {
            once = false;
           
            //if the player sits on switch
            if (other.gameObject.CompareTag("Player"))

            {
                //gets the material name of the switch then removes it
                Renderer checkRender = transform.parent.gameObject.GetComponent<Renderer>();
                Material checkMaterial = checkRender.material;
                Debug.Log(checkMaterial.name);
                Invoke("RemoveSwitch", 1.0f);


                //if this switch relates to the switchwalls
                if (checkMaterial.name == "switch (Instance)")

                {                
                    CreateNewOrangeSwitch();
                    Invoke("RemoveOrangeWalls", 1.0f);

                }

                //if this switch relates to the gamewalls
                if (checkMaterial.name == "walls (Instance)")

                {

                    CreateNewGreenSwitch(checkMaterial);
                    Invoke("RemoveGreenWalls", 1.0f);
                    
                }


            }

        }
        
    }

    //creates a new orange switch
    private void CreateNewOrangeSwitch(){

        //updates new switch location
        float xNewPosition = gameObject.transform.parent.transform.position.x + -6.12f;
        float zNewPosition = gameObject.transform.parent.transform.position.z + 10.2f;
        Vector3 newPosition = new Vector3(xNewPosition, -.22f, zNewPosition);
        
        //instantiates new switch
        GameObject newSwitch = Instantiate(switcher, newPosition, Quaternion.identity);
        newSwitch.transform.name = "orange wall switch";
        
        //gives material a new name
        Renderer render = newSwitch.transform.gameObject.GetComponent<Renderer>();
        render.material.name = "switch (Instance)";
       
    }

    //creates a new green switch
    private void CreateNewGreenSwitch(Material checkMaterial){

        //updates to a new position
        float xNewPosition = gameObject.transform.parent.transform.position.x + 6.12f;
        float zNewPosition = gameObject.transform.parent.transform.position.z + 10.2f;
        Vector3 newPosition = new Vector3(xNewPosition, -.22f, zNewPosition);

        //instantiates the new switch
        GameObject newSwitch = Instantiate(switcher, newPosition, Quaternion.identity);
        newSwitch.transform.name = "green wall switch";

        //changes the material
        Renderer render = newSwitch.transform.gameObject.GetComponent<Renderer>();
        render.material = checkMaterial;

        //gives a new name
        render.material.name = "walls (Instance)";
       
    }

    //destroys switch
    private void RemoveSwitch()
    {

        Destroy(transform.parent.gameObject);
    }

    //removes the orange walls
    private void RemoveOrangeWalls()

    {

        orangeWallContainer.DestroyOrangeWalls();

    }

    //removes the green walls
    private void RemoveGreenWalls()

    {

        level.DestroyGreenWalls();

    }

    
    
}
